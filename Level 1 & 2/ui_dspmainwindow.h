/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QCustomPlot *plot;
    QLineEdit *lineEdit1;
    QLineEdit *lineEdit5;
    QLineEdit *lineEdit4;
    QLineEdit *lineEdit3;
    QLineEdit *lineEdit2;
    QPushButton *plotSignal;
    QPushButton *plotSpectrum;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(504, 403);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        plot = new QCustomPlot(centralWidget);
        plot->setObjectName(QStringLiteral("plot"));

        gridLayout->addWidget(plot, 1, 0, 1, 5);

        lineEdit1 = new QLineEdit(centralWidget);
        lineEdit1->setObjectName(QStringLiteral("lineEdit1"));

        gridLayout->addWidget(lineEdit1, 0, 0, 1, 1);

        lineEdit5 = new QLineEdit(centralWidget);
        lineEdit5->setObjectName(QStringLiteral("lineEdit5"));

        gridLayout->addWidget(lineEdit5, 0, 4, 1, 1);

        lineEdit4 = new QLineEdit(centralWidget);
        lineEdit4->setObjectName(QStringLiteral("lineEdit4"));

        gridLayout->addWidget(lineEdit4, 0, 3, 1, 1);

        lineEdit3 = new QLineEdit(centralWidget);
        lineEdit3->setObjectName(QStringLiteral("lineEdit3"));

        gridLayout->addWidget(lineEdit3, 0, 2, 1, 1);

        lineEdit2 = new QLineEdit(centralWidget);
        lineEdit2->setObjectName(QStringLiteral("lineEdit2"));

        gridLayout->addWidget(lineEdit2, 0, 1, 1, 1);

        plotSignal = new QPushButton(centralWidget);
        plotSignal->setObjectName(QStringLiteral("plotSignal"));

        gridLayout->addWidget(plotSignal, 2, 3, 1, 1);

        plotSpectrum = new QPushButton(centralWidget);
        plotSpectrum->setObjectName(QStringLiteral("plotSpectrum"));

        gridLayout->addWidget(plotSpectrum, 2, 4, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        DSPMainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(DSPMainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DSPMainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DSPMainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DSPMainWindow->setStatusBar(statusBar);

        retranslateUi(DSPMainWindow);

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "DSPMainWindow", Q_NULLPTR));
        plotSignal->setText(QApplication::translate("DSPMainWindow", "Signal", Q_NULLPTR));
        plotSpectrum->setText(QApplication::translate("DSPMainWindow", "Spectrum", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
