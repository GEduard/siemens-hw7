#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::DSPMainWindow)
{
	ui->setupUi(this);
	connect(ui->plotSignal, SIGNAL(clicked()), this, SLOT(PlotSignal()));
	connect(ui->plotSpectrum, SIGNAL(clicked()), this, SLOT(PlotSpectrum()));
}

DSPMainWindow::~DSPMainWindow()
{
	delete ui;
}

void DSPMainWindow::PlotSignal()
{
	//Adding the sine waves togheter
	QVector<double> xAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}

	QVector<double> yAxis(44100);
	for (int i = 0; i < 44100; i++)
	{

		yAxis[i] = 1 * sin(2 * M_PI * ui->lineEdit1->text().toInt() * xAxis[i]) + 
			1 * sin(2 * M_PI * ui->lineEdit2->text().toInt() * xAxis[i]) + 
			1 * sin(2 * M_PI * ui->lineEdit3->text().toInt() * xAxis[i]) + 
			1 * sin(2 * M_PI * ui->lineEdit4->text().toInt() * xAxis[i]) + 
			1 * sin(2 * M_PI * ui->lineEdit5->text().toInt() * xAxis[i]);
	}

	//Plotting the signal
	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxis);
	ui->plot->xAxis->setRange(0, 0.01);
	ui->plot->yAxis->setRange(-5, 5);
	ui->plot->replot();
}

void DSPMainWindow::PlotSpectrum()
{
	//Adding the sine waves togheter
	QVector<double> xAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}

	QVector<double> yAxis(44100);
	for (int i = 0; i < 44100; i++)
	{

		yAxis[i] = 1 * sin(2 * M_PI * ui->lineEdit1->text().toInt() * xAxis[i]) +
			1 * sin(2 * M_PI * ui->lineEdit2->text().toInt() * xAxis[i]) +
			1 * sin(2 * M_PI * ui->lineEdit3->text().toInt() * xAxis[i]) +
			1 * sin(2 * M_PI * ui->lineEdit4->text().toInt() * xAxis[i]) +
			1 * sin(2 * M_PI * ui->lineEdit5->text().toInt() * xAxis[i]);
	}

	//Plotting the frequency domain using FFT
	int FFTSize = 128;
	double frequencyRes = 44100.0 / FFTSize;
	auto fft = Aquila::FftFactory::getFft(FFTSize);
	auto spectrum = fft->fft(yAxis.toStdVector().data());

	int index = 0;
	QVector<double> amplitude, frequency;

	for each(auto item in spectrum)
	{
		amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
		frequency.push_back((index++)*frequencyRes);
	}

	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	ui->plot->graph(0)->setData(frequency, amplitude);
	ui->plot->xAxis->setRange(20, 20000);
	ui->plot->yAxis->setRange(0, *std::max_element(amplitude.begin(), amplitude.end()));
	ui->plot->replot();
}
