#ifndef DSPMAINWINDOW_H
#define DSPMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class DSPMainWindow;
}

class DSPMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DSPMainWindow(QWidget *parent = 0);
    ~DSPMainWindow();

private:
    Ui::DSPMainWindow *ui;

public slots:
	void PlotSpectrum();
	void PlotSignal();
};

#endif // DSPMAINWINDOW_H
