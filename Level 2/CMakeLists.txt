cmake_minimum_required(VERSION 2.8.11)
project(DSPProject)

add_subdirectory(aquila)
include_directories (aquila)
# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# Find the QtWidgets library
find_package(Qt5 COMPONENTS Core Widgets PrintSupport REQUIRED)

file(GLOB INCS "*.h")
file(GLOB SRCS "*.cpp")

# Tell CMake to create the DSPProject executable
#find_package(Qt5PrintSupport REQUIRED)
add_executable(DSPProject WIN32 ${SRCS} ${INCS})
#target_link_libraries(DSPProject Qt5::PrintSupport)

add_custom_command(
    TARGET DSPProject
    PRE_BUILD
    COMMAND uic.exe ${PROJECT_SOURCE_DIR}/dspmainwindow.ui -o ${PROJECT_SOURCE_DIR}/ui_dspmainwindow.h
    COMMENT " Running PRE_BUILD action"
    )
	
# Use the Widgets module from Qt 5.
target_link_libraries(DSPProject Qt5::Widgets Qt5::Core Qt5::PrintSupport ${AQUILA_GENERATED_LIB} ${OOURA_GENERATED_LIB})
